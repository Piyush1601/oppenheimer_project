*** Settings ***
Library  RequestsLibrary
Library  Collections
Library  JSONLibrary
Library  String
Library  ../../../libraries/oppenheimer_base.py
Variables    ../../../config/config.yml
Variables    ../../../api-data/output/calculate-controller.yml
Variables    ../../../api-data/input/herodata.yml
Variables    ../../../util/loggers.yml
Variables    ../../../util/status_code.yml
Test Setup   Session Creation and rakeDatabase

*** Variables ***
${content type}   application/json

*** Test Cases ***
TC_001_Get_Tax_Relief
    ${hero_details}     get_valid_hero_details
    ${working_class}    Convert To Json Format                                        ${hero_details}
    ${response}         Insert a single record of working class hero into database    ${working_class}
    ${excepted_relief}  calculate_tax                                                 ${hero_details}
    ${actual_natid}     ${actual_name}  ${actual_relief}                              Get Tax Relief
    ${excepted_name}    Evaluate        ${hero_details}.get('name')
    ${excepted_natid}   Evaluate        ${hero_details}.get('natid')
    ${encryted_natid}   get_encrypted_natid   ${excepted_natid}
    Should Be Equal     ${actual_name}        ${excepted_name}
    Should Be Equal     ${actual_natid}       ${encryted_natid}
    Should Be Equal     ${actual_relief}      ${excepted_relief}

TC_002_Get_Tax_Relief_With_Higher_Tax
    ${hero_details}     get_second_valid_hero_details
    ${working_class}    Convert To Json Format                                        ${hero_details}
    ${response}         Insert a single record of working class hero into database    ${working_class}
    ${excepted_relief}  calculate_tax                                                 ${hero_details}
    ${actual_natid}     ${actual_name}  ${actual_relief}                              Get Tax Relief
    ${excepted_name}    Evaluate        ${hero_details}.get('name')
    ${excepted_natid}   Evaluate        ${hero_details}.get('natid')
    ${encryted_natid}   get_encrypted_natid   ${excepted_natid}
    Should Be Equal     ${actual_name}        ${excepted_name}
    Should Be Equal     ${actual_natid}       ${encryted_natid}
    Should Be Equal     ${actual_relief}      ${excepted_relief}





*** Keywords ***
Convert To Json Format
    [Arguments]          ${hero_record}
    ${conert_to_json}    Evaluate                 json.dumps(${hero_record})       json
    Return From Keyword  ${conert_to_json}

Session Creation and rakeDatabase
    &{header}            create dictionary        Content-Type=${content type}
    Create Session       Session                  ${calculator controller.baseUrl}
    Post On Session      Session                  ${calculator controller.rakeDatabase}    headers=&{header}

Insert a single record of working class hero into database
    [Arguments]       ${hero_details}
    &{header}         create dictionary   Content-Type=${content type}
    TRY
        ${response}   Post On Session     Session   ${calculator controller.insertPerson}   data=${hero_details}
        ...                                         headers=&{header}    expected_status=Anything
    EXCEPT
        Log To Console   ${connection_failure}
    END
    Return From Keyword    ${response}

Get Tax Relief
    ${response}    GET On Session    Session    ${calculator controller.getTaxRelief}
    ${natid}       Evaluate          ${response.json()[0]}.get('natid')
    ${name}        Evaluate          ${response.json()[0]}.get('name')
    ${relief}      Evaluate          ${response.json()[0]}.get('relief')
    Return From Keyword              ${natid}  ${name}  ${relief}