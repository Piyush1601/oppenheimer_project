*** Settings ***
Library    SeleniumLibrary
Library    String
Test Setup    Open Brower And Open Oppenheimer Portal
Test Teardown    Close All Browsers

*** Variables ***
${url}  http://localhost:8181/

*** Test Cases ***
TC_001_Check_Working_Of_Dispense_Now_Button
    Check Red Color Is Present For Dispense Now Button
    Click Dispense Now Button
    Check If New Page Appear

*** Keywords ***
Open Brower And Open Oppenheimer Portal
    Open Browser    ${url}    Chrome

Click Dispense Now Button
    Click Element    xpath://a[text()='Dispense Now']

Check If New Page Appear
    ${Text}    Get Text    class:container
    Should Be Equal   ${Text}       Cash dispensed

Check Red Color Is Present For Dispense Now Button
    ${ele}    Get Webelement    xpath=//a[@class="btn btn-danger btn-block"]
    ${bg color}    Call Method    ${ele}    value_of_css_property        color
    Should Be Equal   255, 255, 255, 1         ${bg color[5:-1]}