*** Settings ***
Library  RequestsLibrary
Library  Collections
Library  JSONLibrary
Library  String
Library  ../../../libraries/oppenheimer_base.py
Variables    ../../../config/config.yml
Variables    ../../../api-data/output/calculate-controller.yml
Variables    ../../../api-data/input/herodata.yml
Variables    ../../../util/loggers.yml
Variables    ../../../util/status_code.yml
Test Setup   Session Creation and rakeDatabase

*** Variables ***
${content type}   application/json

*** Test Cases ***
TC_001_Insert_Single_Hero_Record_Successfully
    [Tags]             clerks
    ${hero_details}    get_valid_hero_details
    ${working_class}   Convert To Json Format                                        ${hero_details}
    ${response}        Insert a single record of working class hero into database    ${working_class}
    Status Should Be   ${status code.HTTP_ACCEPTED}                                  ${response}
    Should Be Equal    Alright    ${response.text}
    Check If Data Insert In Database    ${hero_details}

TC_002_Insert_Single_Hero_Record_With_Future_Birthday_Year
    ${hero_details}    get_hero_details_with_wrong_bd_year
    ${working_class}   Convert To Json Format                                        ${hero_details}
    ${response}        Insert a single record of working class hero into database    ${working_class}
    Status Should Be   ${status code.HTTP_OK}                                        ${response}

TC_003_Insert_Single_Hero_Record_With_Invalid_Birthday_Month
    ${hero_details}    ${birthday}                                                  get_hero_details_with_wrong_bd_month
    ${working_class}   Convert To Json Format                                       ${hero_details}
    ${response}        Insert a single record of working class hero into database   ${working_class}
    Status Should Be   ${status code.HTTP_INTERNAL_SERVER_ERROR}                    ${response}
    Verify Error Message For Invalid Month  ${response}                             ${birthday}

TC_004_Insert_Single_Hero_Record_With_Invalid_Birthday_Date
    ${hero_details}    ${birthday}                                                  get_hero_details_with_wrong_bd_day
    ${working_class}   Convert To Json Format                                       ${hero_details}
    ${response}        Insert a single record of working class hero into database   ${working_class}
    Status Should Be   ${status code.HTTP_INTERNAL_SERVER_ERROR}                    ${response}
    Verify Error Message For Invalid Day    ${response}                             ${birthday}

TC_005_Insert_Single_Hero_Record_With_More_Char_Gender
    ${hero_details}    get_hero_details_with_gender_more_char
    ${working_class}   Convert To Json Format                                       ${hero_details}
    ${response}        Insert a single record of working class hero into database   ${working_class}
    Status Should Be   ${status code.HTTP_INTERNAL_SERVER_ERROR}                    ${response}
    ${conert_to_json}  Evaluate               json.dumps(${response.text})          json
    ${actual_message}  Evaluate               ${conert_to_json}.get("message")
    Should Be Equal    ${gender scenario.invalid_more_char_gender}                  ${actual_message}

TC_006_Insert_Single_Hero_Record_With_Invalid_Gender
    ${hero_details}    get_hero_details_with_invalid_gender
    ${working_class}   Convert To Json Format                                       ${hero_details}
    ${response}        Insert a single record of working class hero into database   ${working_class}
    Status Should Be   ${status code.HTTP_INTERNAL_SERVER_ERROR}                    ${response}
    ${conert_to_json}  Evaluate               json.dumps(${response.text})          json
    ${actual_message}  Evaluate               ${conert_to_json}.get("message")
    Should Be Equal    ${gender scenario.invalid_more_char_gender}                  ${actual_message}

TC_007_Insert_Single_Hero_Record_With_Invalid_Name
    ${hero_details}    get_hero_details_with_invalid_name
    ${working_class}   Convert To Json Format                                       ${hero_details}
    ${response}        Insert a single record of working class hero into database   ${working_class}
    Status Should Be   ${status code.HTTP_INTERNAL_SERVER_ERROR}                    ${response}
    ${conert_to_json}  Evaluate               json.dumps(${response.text})          json
    ${actual_message}  Evaluate               ${conert_to_json}.get("message")
    Should Be Equal    ${name scenario.invalid_name}                                ${actual_message}

TC_008_Insert_Single_Hero_Record_With_Invalid_Nationalid
    ${hero_details}    get_hero_details_with_invalid_natid
    ${working_class}   Convert To Json Format                                       ${hero_details}
    ${response}        Insert a single record of working class hero into database   ${working_class}
    Status Should Be   ${status code.HTTP_INTERNAL_SERVER_ERROR}                    ${response}
    ${conert_to_json}  Evaluate               json.dumps(${response.text})          json
    ${actual_message}  Evaluate               ${conert_to_json}.get("message")
    Should Be Equal    ${natid scenario.invalid_natid}                              ${actual_message}

TC_009_Insert_Single_Hero_Record_With_Invalid_Salary
    ${hero_details}    get_hero_details_with_invalid_salary
    ${working_class}   Convert To Json Format                                       ${hero_details}
    ${response}        Insert a single record of working class hero into database   ${working_class}
    Status Should Be   ${status code.HTTP_INTERNAL_SERVER_ERROR}                    ${response}
    ${conert_to_json}  Evaluate               json.dumps(${response.text})          json
    ${actual_message}  Evaluate               ${conert_to_json}.get("message")
    Should Be Equal    ${salary scenario.invalid_salary}                            ${actual_message}

TC_010_Insert_Single_Hero_Record_With_Invalid_Tax
    ${hero_details}    get_hero_details_with_invalid_tax
    ${working_class}   Convert To Json Format                                       ${hero_details}
    ${response}        Insert a single record of working class hero into database   ${working_class}
    Status Should Be   ${status code.HTTP_INTERNAL_SERVER_ERROR}                    ${response}
    ${conert_to_json}  Evaluate               json.dumps(${response.text})          json
    ${actual_message}  Evaluate               ${conert_to_json}.get("message")
    Should Be Equal    ${tax scenario.invalid_tax}                                  ${actual_message}

*** Keywords ***
Convert To Json Format
    [Arguments]          ${hero_record}
    ${conert_to_json}    Evaluate                 json.dumps(${hero_record})       json
    Return From Keyword  ${conert_to_json}

Session Creation and rakeDatabase
    &{header}            create dictionary        Content-Type=${content type}
    Create Session       Session                  ${calculator controller.baseUrl}
    Post On Session      Session                  ${calculator controller.rakeDatabase}    headers=&{header}

Insert a single record of working class hero into database
    [Arguments]       ${hero_details}
    &{header}         create dictionary   Content-Type=${content type}
    TRY
        ${response}   Post On Session     Session   ${calculator controller.insertPerson}   data=${hero_details}
        ...                                         headers=&{header}    expected_status=Anything
    EXCEPT
        Log To Console   ${connection_failure}
    END
    Return From Keyword    ${response}

Convert To Json
    [Arguments]          ${hero_details}
    ${conert_to_json}    Evaluate         json.dumps(${hero_details})       json
    Return From Keyword  ${conert_to_json}

Check If Data Insert In Database
    [Arguments]          ${hero_details}
    Create Session       Session              ${calculator controller.base_url}
    ${get_response}      GET On Session       Session                            ${calculator controller.getTaxRelief}
    ${json_object}       Set Variable         ${get_response.json()[0]}
    ${db_name}           Get Value From Json  ${json_object}                     name
    ${request_name}      Evaluate             ${hero_details}.get("name")
    Should Be Equal      ${request_name}        ${db_name[0]}

Verify Error Message For Invalid Month
    [Arguments]          ${response}     ${birthday}
    ${conert_to_json}    Evaluate        json.dumps(${response.text})       json
    ${actual_message}    Evaluate        ${conert_to_json}.get("message")
    ${replace_date}      Replace String  ${birthday scenario.invalid_month}  XXXXXXXX  ${birthday}
    ${expected_message}  Replace String  ${replace_date}                     MM        ${birthday[2:4]}
    Should Be Equal      ${expected_message}    ${actual_message}

Verify Error Message For Invalid Day
    [Arguments]          ${response}     ${birthday}
    ${conert_to_json}    Evaluate        json.dumps(${response.text})       json
    ${actual_message}    Evaluate        ${conert_to_json}.get("message")
    ${replace_date}      Replace String  ${birthday scenario.invalid_date}  XXXXXXXX  ${birthday}
    ${expected_message}  Replace String  ${replace_date}                    DD        ${birthday[0:2]}
    Should Be Equal      ${expected_message}    ${actual_message}

