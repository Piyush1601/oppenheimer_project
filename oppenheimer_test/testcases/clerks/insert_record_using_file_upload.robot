*** Settings ***
Library  RequestsLibrary
Library  Collections
Library  JSONLibrary
Library  String
Library  ../../../libraries/oppenheimer_base.py
Variables    ../../../config/config.yml
Variables    ../../../api-data/output/calculate-controller.yml
Variables    ../../../api-data/input/herodata.yml
Variables    ../../../util/loggers.yml
Variables    ../../../util/status_code.yml
Test Setup   Session Creation and rakeDatabase

*** Variables ***
${headers_file}    multipart/form-data
${content type}    application/json

*** Test Cases ***
TC_001_Insert_Hero_Record_Using_File_Upload_Succesfully
    &{header}       create dictionary              Content-Type=${headers_file}
    ${file}         Get File For Streaming Upload  ${CURDIR}/../../../api-data/input/valid_file_upload_five_records.csv
    ${files}=       CREATE DICTIONARY              file    ${file}
    ${response}     POST On Session    Session     ${calculator controller.uploadFile}    files=${files}
    ${get_response}      GET On Session       Session                            ${calculator controller.getTaxRelief}
    ${length}            Get length           ${get_response.json()}
    ${record present in file}   get_count_from_csv  ${CURDIR}/../../../api-data/input/valid_file_upload_five_records.csv
    Should Be Equal    ${length}    ${record present in file}

TC_002_File_Upload_With_Different_Column_Header_Setting
    &{header}       create dictionary              Content-Type=${headers_file}
    ${file}         Get File For Streaming Upload  ${CURDIR}/../../../api-data/input/incorrect_column_sequence.csv
    ${files}=       CREATE DICTIONARY              file    ${file}
    ${response}     POST On Session    Session     ${calculator controller.uploadFile}    files=${files}
    ${get_response}      GET On Session       Session                            ${calculator controller.getTaxRelief}
    ${length}            Get length           ${get_response.json()}
    ${record present in file}   get_count_from_csv  ${CURDIR}/../../../api-data/input/incorrect_column_sequence.csv
    Should Be Equal    ${length}    ${record present in file}

TC_003_File_Upload_With_One_Column_Missing
    &{header}       create dictionary              Content-Type=${headers_file}
    ${file}         Get File For Streaming Upload  ${CURDIR}/../../../api-data/input/missing_column.csv
    ${files}=       CREATE DICTIONARY              file    ${file}
    ${response}     POST On Session    Session     ${calculator controller.uploadFile}    files=${files}
    Status Should Be   ${status code.HTTP_INTERNAL_SERVER_ERROR}                    ${response}

TC_004_File_Upload_With_Incorrect_Case_Headers
    &{header}       create dictionary              Content-Type=${headers_file}
    ${file}         Get File For Streaming Upload
    ...             ${CURDIR}/../../../api-data/input/incorrect_case_header_upload_five_records.csv
    ${files}=       CREATE DICTIONARY              file    ${file}
    ${response}     POST On Session    Session     ${calculator controller.uploadFile}    files=${files}
    Status Should Be   ${status code.HTTP_INTERNAL_SERVER_ERROR}                    ${response}


*** Keywords ***
Session Creation and rakeDatabase
    &{header}            create dictionary        Content-Type=${content type}
    Create Session       Session                  ${calculator controller.baseUrl}
    Post On Session      Session                  ${calculator controller.rakeDatabase}    headers=&{header}