*** Settings ***
Library  RequestsLibrary
Library  Collections
Library  JSONLibrary
Library  String
Library  ../../../libraries/oppenheimer_base.py
Variables    ../../../config/config.yml
Variables    ../../../api-data/output/calculate-controller.yml
Variables    ../../../api-data/input/herodata.yml
Variables    ../../../util/loggers.yml
Variables    ../../../util/status_code.yml
Test Setup   Session Creation and rakeDatabase

*** Variables ***
${content type}   application/json

*** Test Cases ***
TC_001_Insert_Multiple_Hero_Records_Succesfully
    [Tags]             clerks
    ${hero_details}    Get Request Body
    ${working_class}   Convert To Json Format                                          ${hero_details}
    ${response}        Insert a multiple record of working class hero into database    ${working_class}
    Status Should Be   ${status code.HTTP_ACCEPTED}                                    ${response}
    Should Be Equal    Alright    ${response.text}
    Check If Data Insert In Database    ${hero_details}

TC_002_Insert_Multiple_Hero_Records_With_Same_Natid
    [Tags]             clerks
    ${hero_details}    Get Request Body With Same Natid
    ${working_class}   Convert To Json Format                                          ${hero_details}
    ${response}        Insert a multiple record of working class hero into database    ${working_class}
    Status Should Be   ${status code.HTTP_CONFLICT}                                    ${response}

*** Keywords ***
Session Creation and rakeDatabase
    &{header}            create dictionary        Content-Type=${content type}
    Create Session       Session                  ${calculator controller.base_url}
    Post On Session      Session                  ${calculator controller.rakeDatabase}    headers=&{header}

Get Request Body
    ${first_record}    get_valid_hero_details
    ${second_record}   get_second_valid_hero_details
    @{request_body}    Create List     ${first_record}  ${second_record}
    Return From Keyword    @{request_body}

Get Request Body With Same Natid
    ${first_record}    get_valid_hero_details
    ${second_record}   Set Variable    ${first_record}
    @{request_body}    Create List     ${first_record}  ${second_record}
    Return From Keyword    @{request_body}

Convert To Json Format
    [Arguments]          ${hero_record}
    ${conert_to_json}    Evaluate                 json.dumps(${hero_record})       json
    Return From Keyword  ${conert_to_json}

Insert a multiple record of working class hero into database
    [Arguments]       ${hero_details}
    &{header}         create dictionary   Content-Type=${content type}
    TRY
        ${response}   Post On Session     Session   ${calculator controller.insertMultiple}   data=${hero_details}
        ...                                         headers=&{header}    expected_status=Anything
    EXCEPT
        Log To Console   ${connection_failure}
    END
    Return From Keyword    ${response}

Check If Data Insert In Database
    [Arguments]          ${hero_details}
    Create Session       Session              ${calculator controller.baseUrl}
    ${get_response}      GET On Session       Session                            ${calculator controller.getTaxRelief}
    ${json_object}       Set Variable         ${get_response.json()}
    ${length}            Get length           ${get_response.json()}
    ${db_name}           Get Value From Json  ${json_object}                     name
    Should Be Equal      ${length}            ${multiple record count.two_count}
