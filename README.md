# THE OPPENHEIMER PROJECT

This project will help to calculate taxation relief and dispense the money. 

# Description

In the year 1969, the City of Carson is a growing city that houses close to
millions of working class heroes^. To support the growing city population, a
bill was passed that:
‘each working class hero is to contribute a fraction of their yearly
income towards city building’
This year, as part of the governor’s initiative to pony up surplus cash in the
vault, each working class hero is gifted with taxation relief as recognition
to their voluntary contribution to city building efforts.
To facilitate this, the governor has drafted out the Oppenheimer Project. This
is a software system that has to support 3 features:
- Enable Clerks to populate a list of working class heroes to the system
- Enable Bookkeepers to retrieve the payable taxation relief for each
working class
- Enable Governor to dispense the money to each working class hero at her
discretion


# Getting Started

- Prerequisites

User will required latest version of Python, Robot Framework, Reqquest Library, Selenium

- Installation

User can directly install packages using the file requirement.txt where all the required packages are stored.

- Configuration

User Can install packages by using command - pip install -r /path/to/requirements.txt

# Usage

After opening project in IDE, user need to open the terminal and go to the path of project and need to run below command to run the Project

- run robot file

robot -d results path_to_robot_file.robot

- run robot file based on tag

Below command will run test cases with clerks tag
robot -d results -i clerks path_to_robot_file.robot  

# Roadmap

I have attempted to cover all possible scenarios, and I am confident that we can add more scenarios in Governor, Bookkeepers, and Actuator. However, due to time limitations, I was not able to include all scenarios. Nonetheless, I will continue to update this as I move forward.
