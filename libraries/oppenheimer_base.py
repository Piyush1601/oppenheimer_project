import yaml
from webcolors import rgb_to_name
from yaml import SafeLoader
import random
from dates import dates
from datetime import date
import pandas as pd
d = dates()


def get_valid_hero_details():
    with open('api-data/input/herodata.yml') as f:
        data = yaml.load(f, Loader=SafeLoader)
    hero_details = {
        "birthday": data['valid_data']['record1']['birthday'],
        "gender": data['valid_data']['record1']['gender'],
        "name": data['valid_data']['record1']['name'],
        "natid": str(random.randint(10000, 99999)),
        "salary": data['valid_data']['record1']['salary'],
        "tax": data['valid_data']['record1']['tax']
    }
    return hero_details


def get_second_valid_hero_details():
    with open('api-data/input/herodata.yml') as f:
        data = yaml.load(f, Loader=SafeLoader)
    hero_details = {
        "birthday": data['valid_data']['record2']['birthday'],
        "gender": data['valid_data']['record2']['gender'],
        "name": data['valid_data']['record2']['name'],
        "natid": str(random.randint(1, 9999999999)),
        "salary": data['valid_data']['record2']['salary'],
        "tax": data['valid_data']['record2']['tax']
    }
    return hero_details


def get_hero_details_with_wrong_bd_year():
    valid_hero = get_valid_hero_details()
    future_date = d.get_future_date()
    valid_hero.update({"birthday": str(future_date)})
    return valid_hero


def get_hero_details_with_wrong_bd_month():
    with open('api-data/input/herodata.yml') as f:
        data = yaml.load(f, Loader=SafeLoader)
    valid_hero = get_valid_hero_details()
    valid_hero.update({"birthday": data['invalid_data']['invalid_bday_month']})
    return valid_hero, data['invalid_data']['invalid_bday_month']


def get_hero_details_with_wrong_bd_day():
    with open('api-data/input/herodata.yml') as f:
        data = yaml.load(f, Loader=SafeLoader)
    valid_hero = get_valid_hero_details()
    valid_hero.update({"birthday": data['invalid_data']['invalid_bday_day']})
    return valid_hero, data['invalid_data']['invalid_bday_day']


def get_hero_details_with_gender_more_char():
    with open('api-data/input/herodata.yml') as f:
        data = yaml.load(f, Loader=SafeLoader)
    valid_hero = get_valid_hero_details()
    valid_hero.update({"gender": data['invalid_data']['more_char_gender']})
    return valid_hero


def get_hero_details_with_invalid_gender():
    with open('api-data/input/herodata.yml') as f:
        data = yaml.load(f, Loader=SafeLoader)
    valid_hero = get_valid_hero_details()
    valid_hero.update({"gender": data['invalid_data']['invalid_gender']})
    return valid_hero


def get_hero_details_with_invalid_name():
    with open('api-data/input/herodata.yml') as f:
        data = yaml.load(f, Loader=SafeLoader)
    valid_hero = get_valid_hero_details()
    valid_hero.update({"name": data['invalid_data']['invalid_name']})
    return valid_hero


def get_hero_details_with_invalid_natid():
    with open('api-data/input/herodata.yml') as f:
        data = yaml.load(f, Loader=SafeLoader)
    valid_hero = get_valid_hero_details()
    valid_hero.update({"natid": data['invalid_data']['invalid_natid']})
    return valid_hero


def get_hero_details_with_invalid_salary():
    with open('api-data/input/herodata.yml') as f:
        data = yaml.load(f, Loader=SafeLoader)
    valid_hero = get_valid_hero_details()
    valid_hero.update({"salary": data['invalid_data']['invalid_salary']})
    return valid_hero


def get_hero_details_with_invalid_tax():
    with open('api-data/input/herodata.yml') as f:
        data = yaml.load(f, Loader=SafeLoader)
    valid_hero = get_valid_hero_details()
    valid_hero.update({"tax": data['invalid_data']['invalid_tax']})
    return valid_hero


def calculate_tax(hero_details):
    gender = hero_details['gender']
    salary = hero_details['salary']
    tax = hero_details['tax']
    age = bday_year(date(int(hero_details['birthday'][4:]), int(hero_details['birthday'][2:4]),
                         int(hero_details['birthday'][0:2])))
    variable = get_variable_value(age)
    if gender == 'M':
        gender_bonus = 0
    else:
        gender_bonus = 500
    tax_relief = round(((float(salary) - float(tax)) * variable) + gender_bonus, 2)
    if tax_relief > 0.00 and tax_relief < 50.00:
        tax_relief = 50.00
    return '{:.2f}'.format(tax_relief)


def get_variable_value(age):
    if age <= 18:
        variable = 1
    elif age <= 35:
        variable = 0.8
    elif age <= 50:
        variable = 0.5
    elif age <= 75:
        variable = 0.367
    elif age >= 76:
        variable = 0.05
    return variable


def bday_year(birthdate):
    today = date.today()
    one_or_zero = ((today.month, today.day) < (birthdate.month, birthdate.day))
    year_difference = today.year - birthdate.year
    age = year_difference - one_or_zero
    return age


def get_encrypted_natid(natid):
    length_of_string = len(natid)
    if length_of_string > 4:
        first = list(natid[0:4])
        second = natid[4:]
        third = []
        for i in range(0,len(second)):
            third.append("$")
        final = first + third
    str1= "".join(final)
    return str1


def get_count_from_csv(file):
    results = pd.read_csv(file)
    return len(results)


def get_color_name(rgbcolor):
    return rgb_to_name((rgbcolor))
