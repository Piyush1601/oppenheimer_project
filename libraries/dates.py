from datetime import datetime, timedelta


class dates:
    def get_future_date(self):
        dt = datetime.now()
        td = timedelta(days=4)
        my_date = dt + td
        return my_date.strftime("%d%m%Y")

    def get_current_year(self):
        dt = datetime.now()
        td = timedelta(days=0)
        my_date = dt + td
        return my_date.strftime("%Y")